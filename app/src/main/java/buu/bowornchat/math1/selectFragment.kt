package buu.bowornchat.math1

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.bowornchat.math1.databinding.FragmentGameBinding
import buu.bowornchat.math1.databinding.FragmentSelectBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [selectFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class selectFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentSelectBinding>(inflater,
            R.layout.fragment_select,container,false)
        binding.apply {
            plusbutton.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_selectFragment_to_plusFragment)
            }
            minusbutton.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_selectFragment_to_minusFragment)
            }
            multiplyButton.setOnClickListener{
                requireView().findNavController().navigate(R.id.action_selectFragment_to_multiplyFragment)
            }
        }
        setHasOptionsMenu(true)
        return binding.root
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.
        onNavDestinationSelected(item,requireView().findNavController())
                || super.onOptionsItemSelected(item)
    }
    }



